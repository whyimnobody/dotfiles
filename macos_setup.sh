#!/bin/bash

start=`date +%s`
bold=$(tput bold)
normal=$(tput sgr0)
red=`tput setaf 1`
green=`tput setaf 2`
reset=`tput sgr0`

# Install homebrew requirements
if test ! $(which gcc); then
  echo "Installing command line developer tools..."
  xcode-select --install
fi

# Install Homebrew and associated repos
if test ! $(which brew); then
  echo "Installing homebrew..."
  /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
    brew tap homebrew/cask-versions
    brew tap 'homebrew/bundle'
    brew tap 'homebrew/cask'
    brew tap 'homebrew/cask-drivers'
    brew tap 'homebrew/cask-fonts'
    brew tap 'homebrew/core'
    brew tap 'homebrew/services'
    brew tap aws/tap
    brew tap heroku/brew
fi

# Install ohmyzsh
echo "Install ohmyzsh"
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# Update homebrew and prepare for the coming storm
echo "Updating homebrew..."
brew update
brew upgrade


# General applications
GeneralToolList=(
    qlcolorcode
    qlmarkdown
    quicklook-json
    qlvideo
)
CaskGeneralToolList=(
    firefox
    google-chrome
    iina
    spotify
)
echo "Installing ${bold}General Applications${bold}"
brew install ${GeneralToolList[@]}
brew install --cask ${CaskGeneralToolList[@]}


# Visuals applications
CaskVisualsToolList=(
    fontbase
    gimp
    inkscape
)
echo "Installing ${bold}Visuals Applications${bold}"
brew install --cask ${CaskVisualsToolList[@]}


# Mobile developer applications
CaskMobileDeveloperToolList=(
    fastlane
)
echo "Installing ${bold}Mobile Development Applications${bold}"
brew install --cask ${CaskMobileDeveloperToolList[@]}


# Developer applications
DeveloperUtilitiesList=(
    android-studio
    asdf
    certbot
    contentful-cli
    ctop
    elixir
    fzf
    go
    heroku
    httpie
    httrack
    ipython
    java
    jq
    lnav
    localstack
    magic-wormhole
    netcat
    nmap
    node
    nvm
    pipenv
    peco
    pre-commit
    rbenv
    ruby-build
    tree
    wget
    yarn
    yarn-completion
    zplug
    zsh
    zsh-autosuggestions
    zsh-syntax-highlighting
)
CaskDeveloperUtilitiesList=(
    atom
    cheatsheet
    cyberduck
    # dotnet-sdk
    firefox-developer-edition
    fork
    google-chrome-canary
    iterm2
    maccy
    rectangle
    postman
    wireshark
)
echo "Installing ${bold}Developer Applications${bold}"
brew install ${DeveloperUtilitiesList[@]}
brew install --cask ${CaskDeveloperUtilitiesList[@]}

mkdir ~/.nvm
# TODO: Replace more of this bash junk
echo '
# NVM CONFIG
export NVM_DIR="$HOME/.nvm"
    [ -s "$(brew --prefix)/opt/nvm/nvm.sh" ] && . "$(brew --prefix)/opt/nvm/nvm.sh" # This loads nvm
    [ -s "$(brew --prefix)/opt/nvm/etc/bash_completion.d/nvm" ] && . "$(brew --prefix)/opt/nvm/etc/bash_completion.d/nvm" # This loads nvm bash_completion' >> ~/.bash_profile


echo '
# BASH-COMPLETION CONFIG
[[ -r "$(brew --prefix)/etc/profile.d/bash_completion.sh" ]] && . "$(brew --prefix)/etc/profile.d/bash_completion.sh"' >> ~/.bash_profile


curl -s "https://get.sdkman.io" | bash
echo '
# THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/Users/mpatel/.sdkman"
[[ -s "/Users/mpatel/.sdkman/bin/sdkman-init.sh" ]] && source "${HOME}/.sdkman/bin/sdkman-init.sh"' >> ~/.bash_profile


# Database applications
DatabaseToolList=(
    kafkacat
)
CaskDatabaseToolList=(
    # azure-data-studio
    graphiql
    # pgadmin4
    postico
    studio-3t
    tableplus
)
echo "Installing ${bold}Database Applications${bold}"
brew install ${DatabaseToolList[@]}
brew install --cask ${CaskDatabaseToolList[@]}


# More Developer applications
CaskIDEsList=(
    atom
    pycharm-ce
    vscodium
)
echo "Installing ${bold}Coding Applications${bold}"
brew install --cask ${CaskIDEsList[@]}
cat vscode_extensions.txt | xargs -L1 code --install-extension


# DevOps applications
DevOpsToolList=(
    # ansible
    awscli
    aws-sam-cli
    # consul
    kompose
    # nomad
    # packer
    # terragrunt
    # terraform
    vault
)
CaskDevOpsToolList=(
    docker
    # vagrant
    # vagrant-manager
    # virtualbox
    # vmware-fusion
)
echo "Installing ${bold}DevOps Applications${bold}"
brew install ${DevOpsToolList[@]}
brew install --cask ${CaskDevOpsToolList[@]}

## Install AWS CLI
#pip3 --version
#curl -O https://bootstrap.pypa.io/get-pip.py
#python3 get-pip.py --user
#pip3 install awscli --upgrade --user
aws --version
#rm get-pip.py

curl "https://d1vvhvl2y92vvt.cloudfront.net/awscli-exe-macos.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
aws2 --version


# Productivity applications
ProductivityToolList=(
    zoom
)
CaskProductivityToolList=(
    1password
    authy
    dash
    discord
    # gpg-suite
    keka
    keybase
    notion
    slack
)
echo "Installing ${bold}Productivity Applications${bold}"
brew install ${ProductivityToolList[@]}
brew install --cask ${CaskProductivityToolList[@]}


# Mac/App Store Applications
MacApplicationToolList=(
    409183694 # Keynote
    409203825 # Numbers
    409201541 # Pages
    497799835 # Xcode
    441258766 # Magnet
    # 1450874784 # Transporter
    # 1274495053 # Microsoft To Do
    # 1295203466 # Microsoft Remote Desktop 10
    # 985367838 # Microsoft Outlook
)
echo "Installing ${bold}App Store Applications${bold}"
brew install mas
mas install ${MacApplicationToolList[@]}

echo "######### Save screenshots to ${HOME}/Downloads"
defaults write com.apple.screencapture location -string "${HOME}/Downloads"

echo "######### Save screenshots in PNG format (other options: BMP, GIF, JPG, PDF, TIFF"
defaults write com.apple.screencapture type -string "png"

# Node-based applications
echo "Installing ${bold}NPM Packages${bold}"
npm install --global surge

# Clean up after yourself
brew cleanup

# Config for git
# sh -c 'curl -s https://raw.githubusercontent.com/maxyermayank/developer-mac-setup/master/.gitignore >> ~/.gitignore'
# git config --global push.default current
# git config --global core.excludesfile ~/.gitignore
git config --global user.name ""
git config --global user.email ""
git config --global color.branch auto
git config --global color.diff auto
git config --global color.interactive auto
git config --global color.status auto

# TODO: Add aliases below
# beginDeploy "############# ALIASES #############"
# beginDeploy "############# DOCKER ALIASES #############"
# sh -c 'curl -s https://raw.githubusercontent.com/maxyermayank/developer-mac-setup/master/.docker_aliases >> ~/.docker_aliases'
# source ~/.docker_aliases

§beginDeploy "############# SETUP BASH PROFILE #############"
# TODO: Replace this bash rubbish
# source ~/.bash_profile

runtime=$((($(date +%s)-$start)/60))
beginDeploy "############# Total Setup Time ############# $runtime Minutes"
